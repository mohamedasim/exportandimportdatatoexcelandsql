using System;
using System.Linq;
using ExcelSheetImportExport.Models;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.Windows;
namespace ExcelSheetImportExport.Controllers
{
    [ApiController]
    [Route ("api/excel")]
    public class ImportAndExportController :Controller
    {
        IConfiguration configure;
        public ImportAndExportController(IConfiguration configuration) { configure = configuration; }

        [HttpPost("import")]
        public IActionResult ImportFromExcelToSql([FromForm] FileModel File)
        {

            var list = new List<StudentModel>();
            var stream = new MemoryStream();
            File.file.CopyTo(stream);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(stream);
            var ExcelSheet = package.Workbook.Worksheets[0];
            int rowCount = ExcelSheet.Dimension.Rows;
            for (int i = 2; i <= rowCount; i++)
            {
                list.Add(new StudentModel
                {
                    Id = ExcelSheet.Cells[i, 1].Value.ToString().Trim(),
                    Name = ExcelSheet.Cells[i, 2].Value.ToString().Trim(),
                    DateOfBirth = ExcelSheet.Cells[i, 3].Value.ToString().Trim(),
                    MarksPercentage = ExcelSheet.Cells[i, 4].Value.ToString().Trim(),
                }); ;
            }

            foreach (StudentModel item in list)
            {
                DateTime DOB = DateTime.Parse(item.DateOfBirth);
                string query = "insert into excelrecord values('" + item.Id + "','" + item.Name + "','" + DOB + "','" + item.MarksPercentage + "')";

                string serverPath = configure.GetConnectionString("DefaultConnection");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();
                    }
                }
            }
            return Ok("updated successfully");
        }

        [HttpGet("export")]
        public IActionResult ExportDataFromSqlToExcel()
        {
            try
            {

                string query = "select * from excelrecord";
                DataTable table = new DataTable();
                SqlDataReader reader;
                string server = configure.GetConnectionString("DefaultConnection");
                SqlConnection connection = new SqlConnection(server);
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                DataSet db = new DataSet();
                adapter.Fill(db);
                var stream = new MemoryStream();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage package = new ExcelPackage(stream);
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromDataTable(db.Tables[0], true);
                package.Save();
                stream.Position = 0;
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ExportedFile.xlsx");

            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }

        }
    }
}