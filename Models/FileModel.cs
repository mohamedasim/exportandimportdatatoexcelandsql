using Microsoft.AspNetCore.Http;

namespace ExcelSheetImportExport.Models
{
    public class FileModel
    {
        public IFormFile file{get; set;}
    }
}