using System;
namespace ExcelSheetImportExport.Models
{
    public class StudentModel
    {
        public string Id{get;set;}
        public string Name{get;set;}
        public string DateOfBirth{get;set;}
        public string MarksPercentage{get;set;}
    }
}